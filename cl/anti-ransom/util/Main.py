import time 
from functions import Utils
TAMANO_ARCHIVO=15000
Utils.crearArchivosSenuelo(TAMANO_ARCHIVO)
mapaDeSenuelos=Utils.obtenerMapaDeSenuelos()
mapaDeProcesos = Utils.obtenerMapaProcesos()
Utils.escribirLog("Ordenador protegido", "INFO")
while True:
    procesos=Utils.agregarProcesos(mapaDeProcesos)
    if not Utils.senuelosEstanBien(mapaDeSenuelos):
        Utils.alertaRoja(mapaDeProcesos,TAMANO_ARCHIVO)
    mapaDeProcesos=Utils.mantenimiento(mapaDeProcesos)
    time.sleep(.2)
  