#!/usr/local/bin/python3.9
# -*- coding: utf-8 -*-
'''
Created on Dec 12, 2020

@author: José Miguel Lara Noziglia 
'''
import hashlib
import psutil
import time
from datetime import datetime
from sys import exit
class Utils(object):
    '''
    classdocs
    '''


    def __init__(self, path):
        '''
        initializer
        '''
        self._path=path

    def obtenerMapaDeSenuelos():
        mapaDeSenuelos = {}
        try:
            ficheroDeSenuelos=open("listado_senuelos.cfg","r")
            for line in ficheroDeSenuelos:
                mapaDeSenuelos[line.rstrip()]=Utils.checksumInicial(line.rstrip())
            return mapaDeSenuelos 
        except IOError: 
            Utils.escribirLog("Archivo de configuracion no encontrado: listado_senuelos.cfg","ERROR")
            exit()
            
        
    def checksumInicial(fichero):
        """
        revisa el checksum del fichero especificado.
        """
        try:
            return hashlib.md5(open(fichero,'rb').read()).hexdigest()
        except:
            Utils.escribirLog("Archivo señuelo \""+ fichero + "\" no encontrado.  Revisar archivo de configuración listado_senuelos.cfg.  Saliendo del programa.", "ERROR")
            exit()

    def checksum(fichero):
        """
        revisa el checksum del fichero especificado.
        """
        try:
            return hashlib.md5(open(fichero,'rb').read()).hexdigest()
        except:
            Utils.escribirLog("Archivo señuelo \""+ fichero + "\" no encontrado.", "ADVERTENCIA")
    def obtenerMapaProcesos():
        """
        Obtiene lista de procesos inicial y se le agrega marca de tiempo al proceso.
        """
        procesos = {}
        try:
            pids_iniciales=psutil.pids()
            for pid in pids_iniciales:
                ts = time.time() - 60
                procesos[str(pid)] = ts
            return (procesos)
        except:
            print ("ERROR - getProcesos")

    def agregarProcesos(procesos):
        """
        Agrega procesos a la lista de procesos iniciales con marca de tiempo de cuando fue añadida.
        """
        try:
            pids_iniciales=psutil.pids()
            for pid in pids_iniciales:
                ts = time.time()
                if not str(pid) in procesos:
                    procesos[str(pid)] = ts
            return (procesos)
        except:
            print ("ERROR - agregarProcesos")
    
    def senuelosEstanBien(mapaDeSenuelos):
        for nombreSenuelo in mapaDeSenuelos:
            if mapaDeSenuelos[nombreSenuelo] != Utils.checksum(nombreSenuelo):
                Utils.escribirLog("Checksum de señuelo incorrecto!!!  Alerta de modificación de Archivo: " + nombreSenuelo,"ADVERTENCIA")
                return False
        return True
    
    def alertaRoja(mapaDeProcesos, TAMANO_ARCHIVO):
        """ 
        Se activa el mecanismo de defensa.
        """
        ts = time.time()
        for pid in mapaDeProcesos:
            if (ts-mapaDeProcesos[str(pid)]<10):
                if Utils.procesoExiste(str(pid)):
                    p = psutil.Process(int(pid))
                    try:
                        p.terminate() 
                        Utils.escribirLog(pid + " terminated -  " + str(ts-mapaDeProcesos[str(pid)]),"ERROR")
                    except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                        pass
        Utils.crearArchivosSenuelo(TAMANO_ARCHIVO)
    def escribirLog(mensajeDeLog, tipo):
        """
        Escribir un mensaje en el registro de mensajes (log).
        """
        print (mensajeDeLog)
        ts = time.time() 
        dt_object = datetime.fromtimestamp(ts)
        f= open(str(dt_object.date())+"_anti-ransomware-log.txt","a")
        f.write(tipo + " - " +str(dt_object) + " - " + mensajeDeLog + "\n")
        f.close();
        
    def procesoExiste(pid):
        if psutil.pid_exists(int(pid)):
            return True 
        else:
            False
    def mantenimiento(mapaDeProcesos):
        listadoProcesos=mapaDeProcesos.copy();
        for pid in mapaDeProcesos:
            if not Utils.procesoExiste(str(pid)):
                listadoProcesos.pop(pid)
        return listadoProcesos
    
    def crearArchivosSenuelo(numeroDeLineas):
        try:
            ficheroDeSenuelos=open("listado_senuelos.cfg","r")
            linea=0
            for line in ficheroDeSenuelos:
                linea=linea+1
                ficheroDeSenuelos = open(line.rstrip(),"w")
                if linea%2==0:
                    for i in range(0, numeroDeLineas):
                        ficheroDeSenuelos.write('No modificar este archivo señuelo anti ransomware\n')
                else:
                    ficheroDeSenuelos.write('No modificar este archivo señuelo anti ransomware\n')
                    ficheroDeSenuelos.close()
        except IOError: 
            Utils.escribirLog("Archivo de configuracion no encontrado: listado_senuelos.cfg","ERROR")
            exit()
        finally:
            ficheroDeSenuelos.close()
            
            
            
    if __name__ == '__main__':
        print (__name__)

#        checksum("functions.py")
        obtenerMapaProcesos()
    else:
        print ("Initializing "+__name__)
        